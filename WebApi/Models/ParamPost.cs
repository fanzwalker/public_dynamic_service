﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ParamPost
    {
        [Required]
        public string SpName { get; set; }
        public Dictionary<string, Object> Parameters { get; set;}
    }
}
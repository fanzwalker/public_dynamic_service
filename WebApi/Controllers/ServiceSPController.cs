﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;
using System.Data.Entity.Core.EntityClient;

namespace WebApi.Controllers
{
    [Authorize]
    public class ServiceSPController : ApiController
    {
        public HttpResponseMessage Post(ParamPost paramPost)
        {
            var ConStr = ConfigurationManager.ConnectionStrings["ServiceDBEntities"].ConnectionString;
            EntityConnection ec = new EntityConnection(ConStr);
            SqlConnection conn = new SqlConnection(ec.StoreConnection.ConnectionString);
            try
            {
                if (!ModelState.IsValid) return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Operation Failed");
                using (SqlCommand cmd = new SqlCommand(paramPost.SpName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if(paramPost.Parameters != null)
                    {
                        foreach (KeyValuePair<string, object> pair in paramPost.Parameters)
                        {
                            cmd.Parameters.AddWithValue("@" + pair.Key.ToString(), pair.Value);
                        }
                    }
                    conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        var dt = new DataTable();
                        dt.Load(dr);
                        var dns = new List<dynamic>();
                        foreach (var item in dt.AsEnumerable())
                        {
                            IDictionary<string, object> dn = new ExpandoObject();

                            foreach (var column in dt.Columns.Cast<DataColumn>())
                            {
                                dn[column.ColumnName] = item[column];
                            }

                            dns.Add(dn);
                        }
                        conn.Close();
                        return Request.CreateResponse(HttpStatusCode.OK, dns);
                    }
                    catch(Exception ex)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}

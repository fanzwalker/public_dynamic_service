﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ModelDataAccess;
namespace WebApi.Controllers
{
    public class ParamProgKegMonev
    {
        [Required]
        public string IDPRGRM { get; set; }
        public string TYPE { get; set; }
    }
    public class ProgKegMonevController : ApiController
    {
        public HttpResponseMessage Post(ParamProgKegMonev paramProgKegMonev)
        {
            if (!ModelState.IsValid) return Request.CreateErrorResponse(HttpStatusCode.BadRequest, paramProgKegMonev.IDPRGRM + " Kosong, Harus Diisi");
            try
            {
                using(ServiceDBEntities entitas = new ServiceDBEntities())
                {
                    IEnumerable<DETILPROKEGMONEV> dataResult = entitas.DETILPROKEGMONEVs
                        .Where(e => e.IDPRGRM.Trim() == paramProgKegMonev.IDPRGRM).AsQueryable();
                    var nama = "wew";
                    if(dataResult != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, dataResult);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Kosong");
                    }
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
